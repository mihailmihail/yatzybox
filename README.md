<div align="center">
<h1 align="center">Yatzy.box</h1>
</div>

<div align="center">
<img src="https://mihailmihail.fi/hidden/img/yatsybox_logo2.jpg" alt="App logo" width="50%">
</div>

## Description

Yatzy.box is an android app designed to keep track of your precious Yatzy games. Supports up to 4 players and 5- and 6-dice games.

## How to play

<div align="center">
<img src="https://mihailmihail.fi/hidden/img/yatzybox_basic_usage.gif" alt="App basic usage demo" width="25%">
<img src="https://mihailmihail.fi/hidden/img/yatzybox_sixdice_demo.gif" alt="App basic usage demo" width="25%">
</div>

* Select 5- or 6-dice game mode
* Add players
* Start game!
* You can exit the app at any time, the game will be automatically saved on your phone.

## About the project

Yatzy.box app is build using the latest Android libraries. It follows one activity multiple fragments philosophy and implements the latest navigation principles.

Inspiration for the project came when I was playing yatzy with friends and we ran out of paper score cards.
Naturally we could no longer play the game and our evening was ruined. We searched for a solution online but could not find a product that matched our needs.

## Features

<div align="center">
<img src="https://mihailmihail.fi/hidden/img/yatzybox_navigation.JPG" alt="App navigation" width="50%">
</div>

### Architecture
* Lifecycles
* Livedata
* Navigation
* Room
* ViewModel

### UI
* Animations & Transitions
* Fragment

## Lessons learned
I had no prior experience of developing an Android app and was pleasantly surprised how straight forward the development process was. I was able to get the demo version running quite fast.

However further development proved to be quite challenging because many of the easy to use Android libraries auto generate code and obscured the underlying implementation. Some of the features and their actions were not so clearly documented in the official docs and halfway through the project I realized that the goto language to develop Android apps is Kotlin. However, there was still plenty of resources written in Java although for example some of links listed in the official docs were broken.

In the end the app development process was still a huge success and the desired result was reached. Further development of the app should be effortless, and features can be added with ease. I'm planning on adding statistics and maybe in the future networking support to the app.




