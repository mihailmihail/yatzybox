package com.mcrew.yahtzydotbox.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;

import java.util.List;

import com.mcrew.yahtzydotbox.R;
import com.mcrew.yahtzydotbox.viewmodel.YatzyViewModel;

/**
 * Game list fragment, used for displaying list of all the games saved into the database.
 * - UI: {@link com.mcrew.yahtzydotbox.R.layout#fragment_game_list}
 *  - Button mGoBackButton      - Pops the back stack
 *  - TextView mErrorText       - For error display
 */
public class GameListFragment extends Fragment {

    //private static final String TAG = "FRAGMENT-GAME_LIST";

    private Button mGoBackButton;
    private TextView mErrorText;

    private GameListAdapter mGameListAdapter;

    private YatzyViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mGameListAdapter = new GameListAdapter(inflater);
        return inflater.inflate(R.layout.fragment_game_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mErrorText = view.findViewById(R.id.text_game_select_error);
        mViewModel = new ViewModelProvider(requireActivity()).get(YatzyViewModel.class);
        mViewModel.getAllGames().observe(requireActivity(), this::setGames);

        mGoBackButton = view.findViewById(R.id.button_game_list_back);
        mGoBackButton.setOnClickListener((v) -> {
                Navigation.findNavController(v).popBackStack();
        });

        RecyclerView gameList = view.findViewById(R.id.recycler_view_game_list);

        gameList.setAdapter(mGameListAdapter);
        gameList.setLayoutManager(new LinearLayoutManager(requireActivity()));
    }

    private void setGames(@NonNull List<GameEntity> games) {
        if(games.size() != 0) {
            mErrorText.setVisibility(View.INVISIBLE);
            this.mGameListAdapter.setGames(games);
        } else {
            mErrorText.setVisibility(View.VISIBLE);
            mErrorText.setText(R.string.game_list_error_no_games);
        }
    }

    class GameListAdapter extends RecyclerView.Adapter<GameListAdapter.GameViewHolder> {

        private final LayoutInflater mInflater;
        private List<GameEntity> mPlayedGames = null;

        GameListAdapter(LayoutInflater inflater) {
            mInflater = inflater;
        }

        @NonNull
        @Override
        public GameListAdapter.GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.recyclerview_game_item, parent, false);
            return new GameViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull GameListAdapter.GameViewHolder holder, int position) {
            if (mPlayedGames != null) {
                GameEntity game = mPlayedGames.get(position);
                String text = "Game id:\t" + game.id;
                holder.gameItem.setText(text);
            } else {
                // Covers the case of data not being ready yet.
                String errorText = "Game ####";
                holder.gameItem.setText(errorText);
            }
        }

        @Override
        public int getItemCount() {
            return (mPlayedGames != null) ? mPlayedGames.size() : 0;
        }

        void setGames(List<GameEntity> games) {
            this.mPlayedGames = games;
            notifyDataSetChanged();
        }

        class GameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private final TextView gameItem;

            public GameViewHolder(@NonNull View itemView) {
                super(itemView);
                gameItem = itemView.findViewById(R.id.game_list_item);
                gameItem.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                // TODO: UI -- Popoup modal for selected game (load, inspect, delete)
                GameEntity selectedGame = mPlayedGames.get(getAdapterPosition());
                mViewModel.loadGame(selectedGame);
                NavDirections action = GameListFragmentDirections.actionListToGame();
                Navigation.findNavController(v).navigate(action);
            }
        }
    }
}
