package com.mcrew.yahtzydotbox.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.mcrew.yahtzydotbox.R;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.viewmodel.YatzyViewModel;

import java.util.concurrent.ExecutionException;

/**
 * APP MAIN MENU
 * - UI: {@link com.mcrew.yahtzydotbox.R.layout#fragment_main_menu}
 *  - ImageButton mGameSelectTypeFive   - Select five dice {@link GameEntity.GameType}
 *  - ImageButton mGameSelectTypeSix    - Select six dice {@link GameEntity.GameType}
 *  - Button mGameStart                 - Insert new {@link GameEntity}, navigate to {@link PlayerSelectFragment}
 *  - Button mGame                      - Insert new {@link GameEntity}, navigate to {@link GameListFragment}
 */
public class MainMenu extends Fragment implements View.OnClickListener {

    //private static final String TAG = "FRAGMENT-MAIN_MENU";

    private YatzyViewModel      mViewModel;
    private ImageButton         mGameSelectTypeFive, mGameSelectTypeSix;
    private Button              mGameStart, mGameList;
    private TextView            mErrorText;
    private GameEntity.GameType gameType;
    private int                 mSelectedColor, mUnselectedColor;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel          = new ViewModelProvider(requireActivity()).get(YatzyViewModel.class);
        mGameSelectTypeFive = view.findViewById(R.id.button_game_type_five);
        mGameSelectTypeSix  = view.findViewById(R.id.button_game_type_six);
        mGameStart          = view.findViewById(R.id.button_game_start);
        mGameList           = view.findViewById(R.id.button_show_game_list);
        mErrorText          = view.findViewById(R.id.text_main_menu_error);
        mSelectedColor      = ContextCompat.getColor(requireContext(), R.color.main_menu_selected);
        mUnselectedColor    = ContextCompat.getColor(requireContext(), R.color.main_menu_unselected);
        mGameSelectTypeFive .setOnClickListener(this);
        mGameSelectTypeSix  .setOnClickListener(this);
        mGameStart          .setOnClickListener(this);
        mGameList           .setOnClickListener(this);
        mGameSelectTypeSix  .setColorFilter(mUnselectedColor);
        mGameSelectTypeFive .setColorFilter(mUnselectedColor);
    }

    @Override
    public void onClick(View v) {
        if(v == mGameSelectTypeFive) {
            this.gameType = GameEntity.GameType.FIVE_DICE;
            mGameSelectTypeFive.setColorFilter(mSelectedColor);
            mGameSelectTypeSix.setColorFilter(mUnselectedColor);
            mErrorText.setText("");
        } else if (v == mGameSelectTypeSix) {
            this.gameType = GameEntity.GameType.SIX_DICE;
            mGameSelectTypeSix.setColorFilter(mSelectedColor);
            mGameSelectTypeFive.setColorFilter(mUnselectedColor);
            mErrorText.setText("");
        } else if (v == mGameStart) {
            if(this.gameType == null) {
                mErrorText.setText(R.string.main_menu_error_gametype);
            } else {
                try {
                    mViewModel.insert(this.gameType);
                    NavDirections action = MainMenuDirections.actionMainMenuToPlayerSelect();
                    Navigation.findNavController(v).navigate(action);
                } catch (ExecutionException | InterruptedException e) {
                    e.printStackTrace();
                    mErrorText.setText(R.string.main_menu_error_insert);
                }
            }
        } else if (v == mGameList) {
            Navigation.findNavController(v).navigate(MainMenuDirections.actionMainMenuToGameList());
        }
    }
}