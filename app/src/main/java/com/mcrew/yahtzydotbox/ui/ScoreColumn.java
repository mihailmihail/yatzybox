package com.mcrew.yahtzydotbox.ui;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.mcrew.yahtzydotbox.R;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a player scoreboard layout.
 */
public class ScoreColumn extends ConstraintLayout {

    /**
     * Single score cell.
     */
    class ScoreCell extends androidx.appcompat.widget.AppCompatTextView {

        private final int index;

        public ScoreCell(Context context, int index) {
            super(context);
            this.index = index;
        }

        public int getIndex() {
            return this.index;
        }
    }

    //private final static String TAG = "SCORE_COLUMN";

    public static final int CELL_PLAYER_NAME_INDEX = 0;
    public static final int CELL_BONUS_SCORE_INDEX = 7;

    final String playerName;

    private final int scoreFieldTextColor = Color.rgb(255,255,255);
    private final int scoreFieldBackground = Color.rgb(68, 77, 175);
    private final int scoreFieldBackgroundTint = Color.rgb(58, 67, 165);

    private int[] mChildIds;
    private List<ScoreCell> mScoreCells = new ArrayList<>();
    private byte[] mScoreboard = null;
    private GameEntity.GameType mGameType;

    public ScoreColumn(Context context, String playerName, GameEntity.GameType gameType) {
        super(context);
        this.playerName = playerName;
        this.mGameType = gameType;
        // Add 3 cells for player name, bonus score and total score
        int scoreboardSize = gameType.getScoreBoardSize() + 3;
        this.mChildIds = new int[scoreboardSize];
        LayoutParams params = new LayoutParams(0, LayoutParams.MATCH_PARENT);
        this.setLayoutParams(params);
        this.setId(generateViewId());
        initCells(context);
    }

    /**
     * Sets click listener for each child view.
     * @param listener {@link View.OnClickListener}
     */
    public void setChildOnClickListener(View.OnClickListener listener) {
        // No listener for player name, bonus or total score cell
        for(int i = 0; i < this.getChildCount() - 1; i++) {
            if(i != CELL_PLAYER_NAME_INDEX && i != CELL_BONUS_SCORE_INDEX) {
                this.getChildAt(i).setOnClickListener(listener);
            }
        }
    }

    /**
     * Sets long click listener for each child view.
     * @param listener {@link View.OnLongClickListener}
     */
    public void setChildOnLongClickListener(View.OnLongClickListener listener) {
        // No listener for player name, bonus or total score cell
        for(int i = 0; i < this.getChildCount() - 1; i++) {
            if(i != CELL_PLAYER_NAME_INDEX && i != CELL_BONUS_SCORE_INDEX) {
                this.getChildAt(i).setOnLongClickListener(listener);
            }
        }
    }

    /**
     * Updates scoreboard to UI. Calculates total score based on scoreboard.
     * @param scoreboard New scoreboard to draw.
     */
    public void setScoreBoard(byte[] scoreboard) {
        if(scoreboard == null) {
            return;
        }
        this.mScoreboard = scoreboard;
        int total = 0;
        int score;
        int offset;
        // Start iterating from 1, first one is player name
        // Stop iterating before end of list, last one is total score
        for(int i = 1; i < mScoreCells.size() - 1; i++) {
            // Calc bonus score
            if(i == CELL_BONUS_SCORE_INDEX) {
                if(hasBonus(total)) {
                    total += getBonusScore();
                    mScoreCells.get(i).setText(getBonusScoreTextId());
                } else {
                    mScoreCells.get(i).setText("0");
                }
                continue;
            }

            offset = (i < CELL_BONUS_SCORE_INDEX) ? -1 : -2;
            score = scoreboard[i + offset];

            if(score == Byte.MIN_VALUE) {
                // Score not set
                mScoreCells.get(i).setText("--");
            } else {
                String scoreText = Integer.toString(score);
                mScoreCells.get(i).setText(scoreText);
                total += score;
            }
        }

        // Set total
        String totalText = Integer.toString(total);
        mScoreCells.get(mScoreCells.size() - 1).setText(totalText);
    }

    /**
     * @return Current drawn scoreboard
     */
    public byte[] getScoreboard() {
        return this.mScoreboard;
    }

    /**
     * Construct score cells, apply vertical chain and adds them to parent layout(this)
     * @param context {@link Context}
     */
    private void initCells(Context context) {
        for(int i = 0; i < this.mChildIds.length; i++) {
            ScoreCell scoreCell = new ScoreCell(context, i);
            // Set id
            int id = generateViewId();
            scoreCell.setId(id);
            mChildIds[i] = id;

            // Styling
            int backgroundColor = (i % 2 == 0) ? scoreFieldBackground : scoreFieldBackgroundTint;
            scoreCell.setBackgroundColor(backgroundColor);
            scoreCell.setGravity(Gravity.CENTER);
            scoreCell.setTextColor(scoreFieldTextColor);
            scoreCell.setLayoutParams(new ConstraintLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0));

            // Draw player name
            if(i == 0){
                SpannableString nameString = new SpannableString(playerName);
                nameString.setSpan(new UnderlineSpan(), 0, playerName.length(), 0);
                scoreCell.setText(nameString);
            }

            this.addView(scoreCell);
            mScoreCells.add(scoreCell);
        }

        // Construct constraints
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        View previousItem = null;
        for(ScoreCell scoreCell : mScoreCells) {
            boolean lastItem = mScoreCells.indexOf(scoreCell) == mScoreCells.size() - 1;
            constraintSet.connect(scoreCell.getId(), ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            constraintSet.connect(scoreCell.getId(), ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT);
            if(previousItem == null) {
                constraintSet.connect(scoreCell.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
            } else {
                constraintSet.connect(scoreCell.getId(), ConstraintSet.TOP, previousItem.getId(), ConstraintSet.BOTTOM);
                if(lastItem) {
                    constraintSet.connect(scoreCell.getId(), ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM);
                }
            }
            previousItem = scoreCell;
        }

        // Apply vertical chain
        constraintSet.createVerticalChain(
                ConstraintSet.PARENT_ID,
                ConstraintSet.TOP,
                ConstraintSet.PARENT_ID,
                ConstraintSet.BOTTOM,
                mChildIds,
                null,
                ConstraintSet.CHAIN_SPREAD);

        // Add to layout
        constraintSet.applyTo(this);
    }

    /**
     * Check if score is above bonus threshold.
     * @param score score to check
     * @return true if score is above threshold
     */
    private boolean hasBonus(int score) {
        switch (mGameType) {
            case FIVE_DICE:
                return score > GameEntity.GameType.FIVE_DICE_BONUS_THRESHOLD;
            case SIX_DICE:
                return score > GameEntity.GameType.SIX_DICE_BONUS_THRESHOLD;
            default:
                return false;
        }
    }

    /**
     * Gets the bonus score amount by game type.
     * @return bonus score amount
     */
    private int getBonusScore() {
        switch (mGameType) {
            case FIVE_DICE:
                return GameEntity.FIVE_DICE_BONUS_SCORE;
            case SIX_DICE:
                return GameEntity.SIX_DICE_BONUS_SCORE;
            default:
                return 0;
        }
    }

    /**
     * Gets the bonus score string id by game type.
     * @return Id of bonus score string.
     */
    private int getBonusScoreTextId() {
        switch (mGameType) {
            case FIVE_DICE:
                return R.string.scoreboard_bonus_five_dice;
            case SIX_DICE:
                return R.string.scoreboard_bonus_six_dice;
            default:
                return R.string.game_type_invalid;
        }
    }
}
