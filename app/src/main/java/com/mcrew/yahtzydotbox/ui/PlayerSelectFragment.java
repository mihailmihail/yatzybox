package com.mcrew.yahtzydotbox.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import com.mcrew.yahtzydotbox.R;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.persistence.entity.PlayerEntity;
import com.mcrew.yahtzydotbox.viewmodel.YatzyViewModel;

/**
 * Player select fragment used for adding players into yatzy game.
 * - UI: {@link com.mcrew.yahtzydotbox.R.layout#fragment_select_player}
 */
public class PlayerSelectFragment extends Fragment implements View.OnClickListener {

    //private final static String TAG = "FRAGMENT-PLAYER_SELECT";

    private PlayerListAdapter mPlayerListAdapter;

    private YatzyViewModel mViewModel;

    private Button mButtonAddPlayer;
    private Button mButtonStartGame;
    private Button mButtonGoBack;
    private TextView mErrorText;
    private EditText mTextInput;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback cb = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                mViewModel.abort(getViewLifecycleOwner());
                Navigation.findNavController(getView()).popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, cb);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mPlayerListAdapter = new PlayerListAdapter(inflater);
        return inflater.inflate(R.layout.fragment_select_player, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = new ViewModelProvider(requireActivity()).get(YatzyViewModel.class);
        LiveData<List<PlayerEntity>> players = mViewModel.getPlayers();
        if(players == null) {
            Navigation.findNavController(view).popBackStack();
        } else {
            players.observe(requireActivity(), this::setPlayers);
        }

        RecyclerView playerList = view.findViewById(R.id.recycler_view_game_list);
        playerList.setAdapter(mPlayerListAdapter);
        playerList.setLayoutManager(new LinearLayoutManager(requireActivity()));

        // Buttons
        mButtonAddPlayer = view.findViewById(R.id.button_player_select_add);
        mButtonAddPlayer.setOnClickListener(this);
        mButtonStartGame = view.findViewById(R.id.button_start_game);
        mButtonStartGame.setOnClickListener(this);
        mButtonGoBack = view.findViewById(R.id.button_player_select_back);
        mButtonGoBack.setOnClickListener(this);

        // Error display
        mErrorText = view.findViewById(R.id.text_player_select_error);
        mErrorText.setText(R.string.player_select_add_more);

        // Text input
        mTextInput = new EditText(getActivity());
        mTextInput.setInputType(InputType.TYPE_CLASS_TEXT);
    }

    private void setPlayers(@NonNull List<PlayerEntity> players) {
        mPlayerListAdapter.setPlayers(players);
    }

    @Override
    public void onClick(View v) {
        final DialogInterface.OnClickListener addPlayerButton = ((dialog, which) -> {
            String playerName = mTextInput.getText().toString();
            mViewModel.insert(playerName);
        });

        final DialogInterface.OnClickListener cancelButton = ((dialog, whichButton) -> {
            dialog.dismiss();
        });

        final DialogInterface.OnDismissListener removeParent = ((dialog) -> {
            ((ViewGroup) mTextInput.getParent()).removeView(mTextInput);
            mTextInput.setText("");
        });

        if(v == mButtonAddPlayer) {
            if(!mPlayerListAdapter.isFull()) {
                new AlertDialog.Builder(getContext())
                        .setTitle(R.string.alert_heading_new_player)
                        .setMessage(R.string.alert_message_enter_name)
                        .setView(mTextInput)
                        .setPositiveButton(R.string.button_ok, addPlayerButton)
                        .setNegativeButton(R.string.button_cancel, cancelButton)
                        .setOnDismissListener(removeParent)
                        .show();
                mErrorText.setText("");
            } else {
                mErrorText.setText(R.string.player_select_max_players);
            }
        } else if(v == mButtonStartGame) {
            if(mPlayerListAdapter.hasPlayers()) {
                Navigation
                        .findNavController(v)
                        .navigate(PlayerSelectFragmentDirections.actionPlayerSelectToGame());
            } else {
                mErrorText.setText(R.string.player_select_add_more);
            }
        } else if(v == mButtonGoBack) {
            mViewModel.abort(getViewLifecycleOwner());
            Navigation.findNavController(v).popBackStack();
        }
    }

    static class PlayerListAdapter extends RecyclerView.Adapter<PlayerListAdapter.ViewHolder> {

        private final LayoutInflater mInflater;
        private List<String> players;

        PlayerListAdapter(LayoutInflater layoutInflater) {
            mInflater = layoutInflater;
        }

        @NonNull
        @Override
        public PlayerListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.recyclerview_list_player, parent, false);
            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull PlayerListAdapter.ViewHolder holder, int position) {
            if (players == null) {
                holder.listItem.setText(R.string.player_select_no_player);
            } else {
                String playerText = "Player " + (position + 1) + ": "+ players.get(position);
                holder.listItem.setText(playerText);
            }
        }

        @Override
        public int getItemCount() {
            return (players != null) ? players.size() : 0;
        }

        void setPlayers(List<PlayerEntity> players) {
            if(players.size() != 0) {
                this.players = new ArrayList<>();
                for(PlayerEntity player : players) {
                    this.players.add(player.playerName);
                }
                notifyDataSetChanged();
            }
        }

        boolean hasPlayers() {
            return players != null && players.size() > 0;
        }

        boolean isFull() {
            return players != null && players.size() >= GameEntity.MAX_PLAYERS;
        }

        static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

            private final TextView listItem;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                listItem = itemView.findViewById(R.id.player_select_item);
                listItem.setOnClickListener(this);
                listItem.setOnLongClickListener(this);
            }

            @Override
            public void onClick(View v) {
            }

            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        }
    }
}
