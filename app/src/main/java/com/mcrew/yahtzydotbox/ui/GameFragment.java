package com.mcrew.yahtzydotbox.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.mcrew.yahtzydotbox.R;
import com.mcrew.yahtzydotbox.persistence.entity.PlayerEntity;
import com.mcrew.yahtzydotbox.viewmodel.YatzyViewModel;

/**
 * Yatzy game fragment.
 * A {@link ConstraintLayout} that holds scoreboard categories and player scoreboards.
 * Creates a {@link ScoreColumn} for each player in the game and adds them to the layout.
 * - UI: {@link com.mcrew.yahtzydotbox.R.layout#fragment_game}
 */
public class GameFragment extends Fragment {

    //private static final String TAG = "FRAGMENT-GAME";

    private final List<ScoreColumn> scoreColumns = new ArrayList<>();

    private YatzyViewModel mViewModel;
    private AppCompatEditText mScoreInput;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback cb = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                // TODO: Modal
                mViewModel.abort(getViewLifecycleOwner());
                Navigation.findNavController(getView()).popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, cb);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayList<Integer> childViews = new ArrayList<>();
        List<PlayerEntity> playerList = null;
        Context context = requireContext();

        // Get main parent
        ConstraintLayout gameLayout = view.findViewById(R.id.game_layout);
        // Init score input
        mScoreInput = new AppCompatEditText(context);
        mScoreInput.setInputType(InputType.TYPE_CLASS_NUMBER);
        // Load view model
        mViewModel = new ViewModelProvider(requireActivity()).get(YatzyViewModel.class);
        // Get game type
        GameEntity.GameType mGameType = mViewModel.getGameType();
        // Get scoreboard layout id
        int scoreboardLayout = getScoreboardLayout(mGameType);
        // Init view stub for score categories
        ViewStub stub = view.findViewById(R.id.scoreboard_category);
        stub.setLayoutResource(scoreboardLayout);
        stub.inflate();
        // Add view stub to child view list
        childViews.add(R.id.scoreboard_category_container);

        // Get list of players
        try {
            playerList = mViewModel.getPlayerList();
        } catch (ExecutionException | InterruptedException e) {
            // In case of error, abort and pop the back stack
            e.printStackTrace();
            mViewModel.abort(requireActivity());
            Navigation.findNavController(view).popBackStack();
        }

        // For each player create a score column.
        for(PlayerEntity player : playerList) {
            ScoreColumn scoreColumn = new ScoreColumn(context, player.playerName, mGameType);
            scoreColumn.setChildOnLongClickListener(createLongClickListener(player.playerName));
            scoreColumn.setChildOnClickListener(createClickListener(player.playerName));

            // Get scoreboard and set observer
            mViewModel
                    .getScoreBoard(player.playerName)
                    .observe(requireActivity(), scoreColumn::setScoreBoard);

            // Add to main parent
            gameLayout.addView(scoreColumn);
            scoreColumns.add(scoreColumn);
            childViews.add(scoreColumn.getId());
        }

        // Construct a constraint for child views
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(gameLayout);
        Integer previousId = null;

        for(Integer viewId : childViews) {
            boolean lastItem = childViews.indexOf(viewId) == childViews.size() - 1;
            if(previousId == null) {        // First child
                constraintSet.connect(viewId, ConstraintSet.LEFT, ConstraintSet.PARENT_ID, ConstraintSet.LEFT);
            } else {                        // Other children
                constraintSet.connect(previousId, ConstraintSet.RIGHT, viewId, ConstraintSet.LEFT);
                constraintSet.connect(viewId, ConstraintSet.LEFT, previousId, ConstraintSet.RIGHT);
                if(lastItem) {              // Last child
                    constraintSet.connect(viewId, ConstraintSet.RIGHT, ConstraintSet.PARENT_ID, ConstraintSet.RIGHT);
                }
            }
            previousId = viewId;
        }
        // Chain views together
        constraintSet.createHorizontalChain(ConstraintSet.PARENT_ID,
                ConstraintSet.LEFT,
                ConstraintSet.PARENT_ID,
                ConstraintSet.RIGHT,
                childViews.stream().mapToInt(i -> i).toArray(),
                null,
                ConstraintSet.CHAIN_SPREAD);
        // Apply constraint to main parent
        constraintSet.applyTo(gameLayout);
    }

    /**
     *
     * @param gameType {@link GameEntity.GameType}
     * @return
     */
    private int getScoreboardLayout(GameEntity.GameType gameType) {
        switch (gameType) {
            case SIX_DICE:
                return R.layout.scoreboard_game_type_six;
            default:
                return R.layout.scoreboard_game_type_five;

        }
    }

    private void updateScore(String playerName, int cellIndex, byte score) {
        for(ScoreColumn scoreColumn : scoreColumns) {
            if(scoreColumn.playerName.equals(playerName)) {
                byte[] s = scoreColumn.getScoreboard();
                s[cellIndex] = score;
                mViewModel.updateScore(playerName, s);
            }
        }
    }

    private View.OnLongClickListener createLongClickListener(String playerName) {
        return (v) -> {
            int cellIndex = ((ScoreColumn.ScoreCell) v).getIndex();
            int offset = (cellIndex < ScoreColumn.CELL_BONUS_SCORE_INDEX) ? -1 : -2;
            updateScore(playerName, cellIndex + offset, Byte.MIN_VALUE);
            return true;
        };
    }

    private View.OnClickListener createClickListener(String playerName) {
        return (v) -> {
            final DialogInterface.OnClickListener onClickListener = ((dialog, which) -> {
                if(mScoreInput.getText() != null) {
                    int score = Integer.parseInt(mScoreInput.getText().toString());
                    int cellIndex = ((ScoreColumn.ScoreCell) v).getIndex();
                    int offset = (cellIndex < ScoreColumn.CELL_BONUS_SCORE_INDEX) ? -1 : -2;
                    updateScore(playerName, cellIndex + offset, (byte) score);
                }
            });

            final DialogInterface.OnClickListener onClickListenerNegative = ((dialog, which) -> {
                dialog.dismiss();
            });

            final DialogInterface.OnDismissListener dismissListener = ((dialog) -> {
                ((ViewGroup) mScoreInput.getParent()).removeView(mScoreInput);
                mScoreInput.setText("");
            });

            new AlertDialog.Builder(requireContext())
                    .setTitle(playerName)
                    .setMessage(R.string.alert_message_enter_score)
                    .setView(mScoreInput)
                    .setPositiveButton(R.string.button_ok, onClickListener)
                    .setNegativeButton(R.string.button_cancel, onClickListenerNegative)
                    .setOnDismissListener(dismissListener)
                    .create().show();
        };
    }
}
