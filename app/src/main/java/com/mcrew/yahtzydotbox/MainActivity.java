package com.mcrew.yahtzydotbox;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.mcrew.yahtzydotbox.persistence.YatzyDatabase;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.viewmodel.YatzyViewModel;

import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        YatzyViewModel.Factory factory = new YatzyViewModel.Factory(this);
        YatzyViewModel viewModel =
                new ViewModelProvider(this, factory).get(YatzyViewModel.class);

        // For some reason the viewmodel does not initialize itself if we don't interact with it first.
        // If we don't do this here the first fragment that gets loaded will become viewmodelowner
        // and future viewmodelproviders fail to provide the same viewmodel.
        // Definitely something to look in the future. No way this is the intended behaviour.
        try {
            viewModel.insert(GameEntity.GameType.INVALID_TYPE);
            viewModel.abort(this);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_main);
    }

    public YatzyDatabase getDatabase() {
        return YatzyDatabase.getInstance(this);
    }

    public DataRepository getRepository() {
        return DataRepository.getInstance(getDatabase());
    }
}