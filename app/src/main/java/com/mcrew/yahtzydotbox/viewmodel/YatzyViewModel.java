package com.mcrew.yahtzydotbox.viewmodel;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.mcrew.yahtzydotbox.DataRepository;
import com.mcrew.yahtzydotbox.MainActivity;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.persistence.entity.PlayerEntity;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 *  Viewmodel for Yatzy game.
 *  Can hold the state of currently loaded game.
 */
public class YatzyViewModel extends ViewModel {

    private final static String TAG = "VIEWMODEL-GAME_LIST";

    private final DataRepository mDataRepository;
    private final LiveData<List<GameEntity>> mAllGames;

    private Long selectedGame = null;
    private GameEntity.GameType selectedGameType = null;
    private LiveData<List<PlayerEntity>> currentPlayers = null;
    private boolean isStarted = false;

    public YatzyViewModel(DataRepository dataRepository) {
        mDataRepository = dataRepository;
        mAllGames = mDataRepository.getGames();
    }

    /**
     * Insert new {@link GameEntity} into repository
     * @param gameType {@link GameEntity.GameType}
     * @return long - inserted game id
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public long insert(GameEntity.GameType gameType) throws ExecutionException, InterruptedException {
        this.selectedGame = mDataRepository.insert(new GameEntity(gameType));
        this.selectedGameType = gameType;
        return this.selectedGame;
    }

    /**
     * Insert new {@link PlayerEntity} into repository
     * @param playerName Player name
     */
    public void insert(String playerName) {
        if(getPlayers() != null) {
            byte[] scoreBoard = new byte[this.selectedGameType.getScoreBoardSize()];
            Arrays.fill(scoreBoard, Byte.MIN_VALUE);
            mDataRepository.insert(new PlayerEntity(this.selectedGame, playerName, scoreBoard));
        }
    }

    /**
     * Loads {@link GameEntity} into viewmodel
     * @param game Game entity
     */
    public void loadGame(GameEntity game) {
        this.selectedGame = game.id;
        this.selectedGameType = game.gameType;
        this.isStarted = true;
    }

    /**
     * Returns viewmodel to initial state.
     * Deletes unstarted game from database and disconnects observers from player list.
     */
    public void abort(LifecycleOwner owner) {
        try {
            if(this.selectedGame != null) {
                GameEntity game = getGame(this.selectedGame);
                if(game != null && !isStarted) {
                    // Remove unstarted game from database
                    mDataRepository.deleteGame(this.selectedGame);
                    // Remove players from unstarted game
                    mDataRepository.deletePlayersByGameId(this.selectedGame);
                }
            }
            // Disconnect observers
            if(this.currentPlayers != null) {
                this.currentPlayers.removeObservers(owner);
            }
        } catch (ExecutionException | InterruptedException | NullPointerException e) {
            Log.println(Log.DEBUG, TAG, e.toString());
            // Delete failed, probably some garbage in the database now
        } finally {
            // Reset to initial state
            this.selectedGame = null;
            this.currentPlayers = null;
            this.isStarted = false;
        }
    }

    /**
     * Gets a {@link GameEntity} from repository with the given id
     * @param id game id to get
     * @return {@link GameEntity}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public GameEntity getGame(long id) throws ExecutionException, InterruptedException {
        return mDataRepository.getGame(id);
    }

    /**
     * @return {@link LiveData} of all games in repository
     */
    public LiveData<List<GameEntity>> getAllGames() {
        return mAllGames;
    }

    /**
     * @return Currently loaded game player list as a {@link LiveData} object.
     *          Returns null if no game is currently selected.
     */
    public LiveData<List<PlayerEntity>> getPlayers() {
        if(this.selectedGame == null) {
            this.currentPlayers = null;
        } else if (this.currentPlayers == null) {
            this.currentPlayers = mDataRepository.getPlayers(this.selectedGame);
        }

        return this.currentPlayers;
    }

    /**
     *
     * @return Currently loaded game player list
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public List<PlayerEntity> getPlayerList() throws ExecutionException, InterruptedException {
        return mDataRepository.getPlayerList(this.selectedGame);
    }

    /**
     * @return Current game {@link GameEntity.GameType}
     */
    public GameEntity.GameType getGameType() {
        if(this.selectedGame != null) {
            return this.selectedGameType;
        } else {
            return GameEntity.GameType.INVALID_TYPE;
        }
    }

    /**
     * @param playerName Name of the player whose scoreboard to get.
     * @return {@link LiveData} object of the players scoreboard
     */
    public LiveData<byte[]> getScoreBoard(String playerName) {
        return mDataRepository.getScoreboard(this.selectedGame, playerName);
    }

    /**
     * Update player scoreboard
     * @param playerName Player whose scoreboard to update
     * @param scoreboard Updated scoreboard
     */
    public void updateScore(String playerName, byte[] scoreboard) {
        // Once first scoreboard is updated, the game is considered 'started'
        if(!this.isStarted) {
            // TODO: update GameEntity if needed
            this.isStarted = true;
        }
        mDataRepository.updateScoreboard(this.selectedGame, playerName, scoreboard);
    }

    public static class Factory implements ViewModelProvider.Factory {
        private final DataRepository repository;

        public Factory(MainActivity activity) {
            this.repository = activity.getRepository();
        }

        @SuppressWarnings("unchecked")
        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new YatzyViewModel(repository);
        }
    }
}
