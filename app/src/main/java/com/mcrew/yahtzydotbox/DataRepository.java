package com.mcrew.yahtzydotbox;

import androidx.lifecycle.LiveData;

import com.mcrew.yahtzydotbox.persistence.YatzyDatabase;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.persistence.entity.PlayerEntity;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class DataRepository {

    private static final String TAG = "DATA_REPOSITORY";

    private static DataRepository sInstance;

    private final YatzyDatabase mDatabase;

    private final LiveData<List<GameEntity>> mAllGames;

    private DataRepository(final YatzyDatabase database){
        mDatabase = database;
        mAllGames = database.yatzyDao().getAllGames();
    }

    /**
     * Gets the current instance of {@link DataRepository}, initializes a new one if current instance is null
     * @param database {@link YatzyDatabase} database
     * @return Current instance of {@link DataRepository}
     */
    public static DataRepository getInstance(final YatzyDatabase database) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(database);
                }
            }
        }
        return sInstance;
    }

    /**
     * Insert {@link GameEntity} into yatzy dao
     * @param game {@link GameEntity} game entity to insert
     * @return Inserted game id.
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public long insert(GameEntity game) throws ExecutionException, InterruptedException {
        return YatzyDatabase.databaseWriteExecutor
                .submit(() -> mDatabase.yatzyDao().insert(game)).get();
    }

    /**
     * Insert {@link PlayerEntity} into yatzy dao
     * @param player {@link PlayerEntity} player entity to insert
     */
    public void insert(PlayerEntity player) {
        Runnable r = () -> mDatabase.yatzyDao().insert(player);
        YatzyDatabase.databaseWriteExecutor.submit(r);
    }

    /**
     * Delete game by id from yatzy dao
     * @param id Id of the game to be deleted
     */
    public void deleteGame(long id) {
        YatzyDatabase.databaseWriteExecutor.submit(() -> {
            mDatabase.yatzyDao().deleteGameById(id);
        });
    }

    /**
     * Delete all players by game id from yatzy dao
     * @param id game id
     */
    public void deletePlayersByGameId(long id) {
        YatzyDatabase.databaseWriteExecutor.submit(() -> {
            mDatabase.yatzyDao().deletePlayersByGameId(id);
        });
    }

    /**
     * Get {@link LiveData} object of player scoreboard
     * @param gameId Game id
     * @param playerName Name of the player whose scoreboard to get.
     * @return
     */
    public LiveData<byte[]> getScoreboard(long gameId, String playerName) {
        return mDatabase.yatzyDao().getScoreboard(gameId, playerName);
    }

    /**
     * Update player scoreboard.
     * @param gameId Game id
     * @param playerName Name of the player whose scoreboard to update
     * @param scoreboard Updated scoreboard
     */
    public void updateScoreboard(long gameId, String playerName, byte[] scoreboard) {
        YatzyDatabase.databaseWriteExecutor.submit(() -> {
            mDatabase.yatzyDao().updateScoreboard(gameId, playerName, scoreboard);
        });
    }

    /**
     * Get {@link LiveData} list of all games in yatzy dao.
     * @return {@link LiveData} list of all games in yatzy dao
     */
    public LiveData<List<GameEntity>> getGames() {
        return mAllGames;
    }

    /**
     * Get a {@link LiveData} list of all the players in a game
     * @param gameId Id of the game
     * @return {@link LiveData} list of all the players
     */
    public LiveData<List<PlayerEntity>> getPlayers(long gameId) {
        return mDatabase.yatzyDao().getPlayersByGameId(gameId);
    }

    /**
     * Get a {@link List} of {@link PlayerEntity} in a game.
     * @param gameId Id of the game
     * @return {@link List} of {@link PlayerEntity}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public List<PlayerEntity> getPlayerList(long gameId) throws ExecutionException, InterruptedException {
        return YatzyDatabase.databaseWriteExecutor
                .submit(() -> mDatabase.yatzyDao().getPlayerListByGameId(gameId)).get();
    }

    /**
     * Get {@link GameEntity} by id.
     * @param id Game id
     * @return {@link GameEntity}
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public GameEntity getGame(long id) throws ExecutionException, InterruptedException {
        return YatzyDatabase.databaseWriteExecutor
                .submit(() -> mDatabase.yatzyDao().getGame(id)).get();
    }
}
