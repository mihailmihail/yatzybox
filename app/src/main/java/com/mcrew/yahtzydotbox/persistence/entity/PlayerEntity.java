package com.mcrew.yahtzydotbox.persistence.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.mcrew.yahtzydotbox.persistence.Converters;

/**
 * Represents a player in a game. Room does not support relation in a easy to implement way, so we
 * create new players for each game, not optimal but works. We use byte array for keeping track of
 * the scores because it enables us to easily save it into database without extra converters. This
 * should not cause any problems since any score in yatzy is not greater than 127 (or less than -128)
 * and fields that exceed these values, mainly the total score field, are calculated in runtime and
 * not saved into the database. This will create problems if we want to support custom rulesets that
 * have scores exceeding 127.
 */
@Entity(tableName = "players",
        primaryKeys = {"gameId", "playerName"})
@TypeConverters({Converters.class})
public class PlayerEntity {

    public long gameId;
    public byte[] scoreboard;
    @NonNull
    public String playerName;

    public PlayerEntity(long gameId, @NonNull String playerName, byte[] scoreboard) {
        this.gameId = gameId;
        this.playerName = playerName;
        this.scoreboard = scoreboard;
    }
}
