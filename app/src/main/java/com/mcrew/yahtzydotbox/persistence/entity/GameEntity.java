package com.mcrew.yahtzydotbox.persistence.entity;

import androidx.annotation.IntegerRes;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.mcrew.yahtzydotbox.persistence.Converters;

@Entity(tableName = "games")
@TypeConverters({Converters.class})
public class GameEntity {

    @Ignore
    public final static int MAX_PLAYERS = 4;
    @Ignore
    public final static int FIVE_DICE_BONUS_SCORE = 50;
    @Ignore
    public final static int SIX_DICE_BONUS_SCORE = 100;

    @PrimaryKey(autoGenerate = true)
    public long id;

    public boolean isStarted = false;

    public GameType gameType;

    public GameEntity(GameType gameType) {
        this.gameType = gameType;
    }

    public enum GameType {
        FIVE_DICE(0),
        SIX_DICE(1),
        INVALID_TYPE(-1);

        public final static int FIVE_DICE_BONUS_THRESHOLD = 62;
        public final static int SIX_DICE_BONUS_THRESHOLD = 83;

        public final int value;

        GameType(int value) {
            this.value = value;
        }

        public int getScoreBoardSize() {
            switch (this.value) {
                case 0:
                    return 15;
                case 1:
                    return 20;
                default:
                    return 0;
            }
        }
    }
}
