package com.mcrew.yahtzydotbox.persistence;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.mcrew.yahtzydotbox.persistence.dao.YatzyDao;
import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.persistence.entity.PlayerEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {GameEntity.class, PlayerEntity.class}, exportSchema = false, version = 4)
@TypeConverters({Converters.class})
public abstract class YatzyDatabase extends RoomDatabase {

    private static final String DATABASE_NAME       = "yatzy_database";
    private static final int    NUMBER_OF_THREADS   = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    private static YatzyDatabase INSTANCE;

    public abstract YatzyDao yatzyDao();

    public static YatzyDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            synchronized (YatzyDatabase.class) {
                if (INSTANCE == null) {
                    RoomDatabase.Callback callback = new RoomDatabase.Callback() {

                        @Override
                        public void onOpen(@NonNull SupportSQLiteDatabase db) {
                            super.onOpen(db);

                            /* D E V */
/*                            databaseWriteExecutor.execute(() -> {
                                YatzyDao dao = INSTANCE.yatzyDao();
                                dao.deletePlayers();
                                dao.deleteGames();
                            });*/
                        }
                    };

                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            YatzyDatabase.class, DATABASE_NAME).addCallback(callback).build();
                }
            }
        }
        return INSTANCE;
    }
}
