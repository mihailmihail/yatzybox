package com.mcrew.yahtzydotbox.persistence;

import androidx.room.TypeConverter;

import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;

public class Converters {

/*    @TypeConverter
    public static byte[] scoreboardFromString(String value) {
        return Base64.getDecoder().decode(value);
    }

    @TypeConverter
    public static String scoreboardToString(byte[] scoreboard) {
        return Base64.getEncoder().encodeToString(scoreboard);
    }*/

    @TypeConverter
    public static int fromGameType(GameEntity.GameType gameType) {
        return gameType.value;
    }

    @TypeConverter
    public static GameEntity.GameType toGameType(int value) {
        switch (value) {
            case 0:
                return GameEntity.GameType.FIVE_DICE;
            case 1:
                return GameEntity.GameType.SIX_DICE;
            default:
                return GameEntity.GameType.INVALID_TYPE;
        }
    }
}
