package com.mcrew.yahtzydotbox.persistence.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.mcrew.yahtzydotbox.persistence.entity.GameEntity;
import com.mcrew.yahtzydotbox.persistence.entity.PlayerEntity;

import java.util.List;

@Dao
public interface YatzyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(GameEntity game);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PlayerEntity player);

    @Query("DELETE FROM games WHERE id = :id")
    void deleteGameById(long id);

    @Query("DELETE FROM players WHERE gameId = :gameId")
    void deletePlayersByGameId(long gameId);

    @Query("SELECT * from games")
    LiveData<List<GameEntity>> getAllGames();

    @Query("SELECT * FROM games WHERE id = :gameId")
    GameEntity getGame(long gameId);

    @Query("SELECT gameType FROM games WHERE id = :gameId")
    GameEntity.GameType getGameTypeById(long gameId);

    @Transaction
    @Query("SELECT scoreboard FROM players WHERE gameId=:gameId AND playerName=:playerName")
    LiveData<byte[]> getScoreboard(long gameId, String playerName);

    @Transaction
    @Query("UPDATE players SET scoreboard =:scoreBoard WHERE gameId=:gameId AND playerName=:playerName")
    void updateScoreboard(long gameId, String playerName, byte[] scoreBoard);

    @Transaction
    @Query("SELECT * FROM players WHERE gameId=:gameId")
    LiveData<List<PlayerEntity>> getPlayersByGameId(long gameId);

    @Query("SELECT * FROM players WHERE gameId=:gameId")
    List<PlayerEntity> getPlayerListByGameId(long gameId);

    @Query("DELETE FROM games")
    void deleteGames();

    @Query("DELETE FROM players")
    void deletePlayers();
}
